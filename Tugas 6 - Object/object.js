// 1
console.log("--1--")
function arrayToObject(arr) {
    // Code di sini 
    for(i = 0; i < arr.length; i++){
        var year = new Date().getFullYear();
        var people = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: !arr[i][3] || arr[i][3] > year ? "Invalid Birth Year" : arr[i][3]
        }
        console.log(i+1 + '. ' + arr[i][0] + ' ' + arr[i][1] + ':')
        console.log(people)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


// 2
console.log("\n--2--")
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if(money < 50000) return "Mohon maaf, uang tidak cukup";
    var invoice = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney: 0
    }
    if(money >= 1500000){
        invoice.listPurchased.push("Sepatu Stacattu");
        money -= 1500000;
    }
    if(money >= 500000){
        invoice.listPurchased.push("Baju Zoro");
        money -= 500000;
    }
    if(money >= 250000){
        invoice.listPurchased.push("Baju H&N");
        money -= 250000;
    }
    if(money >= 175000){
        invoice.listPurchased.push("Sweater Unikloog");
        money -= 175000;
    }
    if(money >= 50000){
        invoice.listPurchased.push("Casing HP");
        money -= 50000;
    }
    invoice.changeMoney = money;
    return invoice;


  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


//   3
console.log("\n--3--");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    arrOut = [];
    arrPenumpang.forEach(row => {
        var out = {
            penumpang: row[0],
            naikDari: row[1],
            tujuan: row[2],
            bayar: (rute.indexOf(row[2]) - rute.indexOf(row[1])) * 2000
        }
        arrOut.push(out);
    });
    return arrOut;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]