// No 1

var nama = "John"
var peran = "   "

if(!nama) console.log("Nama harus diisi!")
else{
    if(!peran) console.log("Halo "+ nama + ", pilih peranmu untuk memulai game!")
    else{
        console.log("Selamat datang di dunia werewolf, " + nama)
        if(peran == "Penyihir"){
            console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
        }
        else if(peran == "Guard"){
            console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
        }
        else if(peran == "Werewolf"){
            console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
        }
    }
}

console.log('\n')
// No 2
var hari = 21; 
var bulan = 9; 
var tahun = 1945;
var stringBulan;

switch(bulan){
    case 1 : {stringBulan = "Januari"; break;}
    case 2 : {stringBulan = "Februari"; break;}
    case 3 : {stringBulan = "Maret"; break;}
    case 4 : {stringBulan = "April"; break;}
    case 5 : {stringBulan = "Mei"; break;}
    case 6 : {stringBulan = "Juni"; break;}
    case 7 : {stringBulan = "Juli"; break;}
    case 8 : {stringBulan = "Agustus"; break;}
    case 9 : {stringBulan = "September"; break;}
    case 10 : {stringBulan = "Oktober"; break;}
    case 11 : {stringBulan = "November"; break;}
    case 12 : {stringBulan = "Desember"; break;}
}
console.log(hari + " " + stringBulan + " " + tahun);