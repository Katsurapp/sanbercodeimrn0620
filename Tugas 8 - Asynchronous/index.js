// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var index = 0;

function lanjutBaca(sisaWaktu){
    index++;
    if(index < books.length){
        readBooks(sisaWaktu, books[index], lanjutBaca)
    }
}
readBooks(10000, books[index], lanjutBaca)