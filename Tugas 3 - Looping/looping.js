console.log("--1--\nLOOPING PERTAMA")
var i = 2;
while(i <= 20) {
    console.log(i + " - I love coding")
    i+=2
}

console.log("LOOPING KEDUA")
while(i > 0){
    console.log(i + " - I will become a mobile developer")
    i-=2
}

console.log("\n--2--")
for(i = 1; i <= 20; i++){
    var out = i + " - "
    if(i % 2 == 0){
        out += "Berkualitas"
    }
    else{
        if(i % 3 == 0) out += "I Love Coding"
        else out += "Santai"
    }

    console.log(out)
}

console.log("\n--3--")
for(i = 0; i < 4; i ++){
    console.log("#".repeat(8))
}



console.log("\n--4--")
for(i = 0; i < 7; i++){
    console.log("#".repeat(i+1))

}

console.log("\n--5--")
for(i = 0; i < 8; i++){
    if(i % 2 == 0) console.log("# ".repeat(4))
    else console.log(" #".repeat(4))
}