// 1
console.log("--1--")
function range(x, y){
    var arr = [];
    if(!x || !y){return -1;}
    else{
        if(y >= x){
            for(var i = x; i <= y; i++){
                arr.push(i);
            }
        }else{
            for(var i = x; i >= y; i--){
                arr.push(i);
            }
    }
}
    return arr;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 



// 2
console.log("\n--2--");

function rangeWithStep(x, y, step){
    var arr = [];
    if(!x || !y){return -1;}
    else{
        if(y >= x){
            for(var i = x; i <= y; i+=step){
                arr.push(i);
            }
        }else{
            for(var i = x; i >= y; i-=step){
                arr.push(i);
            }
    }
}
    return arr;
}

 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// 3
console.log("\n--3--");

function sum(x,y,step){
    if(!x) return 0;
    else{
        if(!y) return x;
        else{
            var sum = 0;
            if(!step) step = 1;
            if(y >= x){
                for(var i = x; i <= y; i+=step){
                    sum += i;
                }
            }else{
               for(var i = x; i >= y; i-=step) {
                   sum += i;
               }
            }
            return sum;
        }
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 



// 4
console.log("\n--4--");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

for(var i = 0; i < input.length; i++){
    console.log(`Nomor ID: ${input[i][0]}`);
    console.log(`Nama Lengka: ${input[i][1]}`);
    console.log(`TTL: ${input[i][2]} ${input[i][3]}`);
    console.log(`Hobi: ${input[i][4]}\n`);
    
}

// 5
console.log("--5--")

function balikKata(str){
    var out = "";
    for(var i = str.length - 1; i >= 0; i--){
        out += str[i];
    }
    return out;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// 6
console.log("\n--6--")


function dataHandling2(arr){
    arr[1] = arr[1] + "Elsharawy";
    arr[2] = "Provinsi " + arr[2];
    arr[4] = "Pria";
    arr.push("SMA Internasional Metro");
    console.log(arr);

    var tgl = arr[3].split("/");
    var stringBulan ="";
    switch(tgl[1]){
        case "01" : {stringBulan = "Januari"; break;}
        case "02" : {stringBulan = "Februari"; break;}
        case "03" : {stringBulan = "Maret"; break;}
        case "04" : {stringBulan = "April"; break;}
        case "05" : {stringBulan = "Mei"; break;}
        case "06" : {stringBulan = "Juni"; break;}
        case "07" : {stringBulan = "Juli"; break;}
        case "08" : {stringBulan = "Agustus"; break;}
        case "09" : {stringBulan = "September"; break;}
        case "10" : {stringBulan = "Oktober"; break;}
        case "11" : {stringBulan = "November"; break;}
        case "12" : {stringBulan = "Desember"; break;}
    }
    console.log(stringBulan)
    console.log(tgl.sort(function(a,b){return(b-a)})) 
    console.log(tgl.join("-"))
    console.log(arr[1].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
// Soal kurang jelas, mohon selanjutnya lebih diperjelas lagi
// Sort tidak dijelaskan berdasarkan integer, tapi pada soal output yg diminta adl hasil sort berdasarkan integer
