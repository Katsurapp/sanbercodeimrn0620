// 1
console.log("--1--")

class Animal {
    // Code class di sini
    name;
    legs = 4;
    cold_blooded = false;
    
    constructor(name){
        this.name = name;
    
    }
    
    get name(){
        return this.name;
    }

    get legs(){
        return this.legs;
    }

    get cold_blooded(){
        return this.cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false




class Ape extends Animal {
    constructor(name){
        super(name)
        this.legs = 2;
    }

    yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name){
        super(name)
    }

    jump(){
        console.log("hop hop")
    }
}

 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



// 2
console.log("\n--2--")
class Clock {
    timer;
    static template;
    constructor(o){
        this.template = o.template;
    }

    render = function(template = this.template) {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
    }
    
    stop = function() {
        clearInterval(this.timer);
    };
    
    start = function() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  